from django import forms

from .models import Review


class ReviewForm(forms.ModelForm):
    """формы отзывов"""
    class Meta:
        model = Review
        fields = ("name", "email", "text")